=== Gallery by Supsystic ===
Contributors: supsystic.com
Donate link: http://supsystic.com/plugins/gallery
Tags: gallery, photo gallery, responsive, foto gallery, grid, grid gallery, add gallery, album, foto, fotoalbum, galery, image, photo, gird, website gallery, wordpress gallery, gallery plugin, image gallery, responsive gallery, post feed gallery
Tested up to: 4.3
Stable tag: 1.4.2

Ultimate Grid Photo Gallery with visual editor to build photo gallery skins. Responsive mobile ready gallery to show images, posts and videos

== Description ==

= WordPress Photo Gallery by Supsystic =

* [Responsive WordPress Gallery](http://supsystic.com/plugins/photo-gallery?utm_source=wordpress&utm_medium=responsive&utm_campaign=gallery "Responsive WordPress Gallery")
* [Photo Gallery Demo](http://supsystic.com/gallery-examples?utm_source=wordpress&utm_medium=demo&utm_campaign=gallery "WordPress Photo Gallery")
* [Grid Gallery FAQ and Documentation](http://supsystic.com/plugins/photo-gallery/#faq "Grid Gallery FAQ and Documentation")

[Grid Gallery WordPress Plugin](http://supsystic.com/plugins/photo-gallery?utm_source=wordpress&utm_medium=description&utm_campaign=gallery "Photo Gallery WordPress Plugin") by Supsystic with a great number of layouts will help you to create quality respectable portfolios and image galleries. Its unlimited options allow to choose different designs and styles, which make your photo grid gallery attractive for users.

* Elegant photo gallery design optimized for mobile devices
* Full customization image gallery styles such as shadow, border and caption
* Horizontal, Vertical and Fixed gallery layouts
* Add links, icons and captions
* Post Feed Gallery and Video Gallery
* Carousel gallery
* HTML caption option
* Available with any WordPress Theme

Gallery plugin will help you display your images for maximum effect on your blog or website.

A photo gallery adds an instant professional touch to any WordPress website. But it doesn't just enhance your sites looks, it also makes image management a breeze, by allowing bulk uploading and publishing.

[youtube https://www.youtube.com/watch?v=5bkjrlV14CE#t=14]

Image Gallery by Supsystic is an easy to use gallery plugin that allows you to add simply galleries to your website. The plugin utilizes the WordPress media uploader for images.

Whether you are showcasing images, video or HTML, this gallery will render them beautifully on any platform and any browser size. Variety of responsive image gallery effects and gallery slideshows to better manage and display your photos, screenshots and other forms of image gallery on your site. Some incorporate transition effects, fade-in, fade-out and other effects, as well as thumbnail views of the gallery for faster access, flexibility and ease-of-use to view your site in full all-out browser mode and small-sized smartphone screens.

To make it easier for you to find the right pick for your websites needs, we have put together a presets photo gallery templates you can choose them when create new gallery and later customize design.
 
= Translations gallery in your language =

You have an incredible opportunity to get PRO version for free. Make Translation of the image gallery plugin! It will be amazing if you take advantage of this offer!
 
* English
* French
* Dutch
* Finnish
* Portuguese (Brazil)


== Installation ==

= First time Gallery by Supsystic user =

Thank you for choosing Gallery by Supsystic! Open page of our plug-in admin panel WordPress and you will see two menu items: "New Gallery" and "Galleries".

In order to create your photo gallery, simply click on "New Gallery" in your left navigation menu.  When Create New Gallery page loads, set the name of gallery, choose gallery template and click "Save" button. On the next step you can change template and settings of your gallery. To add images to your photo gallery click on button "Add Images" and choose images from WordPress Media Library. In order to manage images click the "Image List" button. Here you can also set image caption and many more.
On the Main tab you can set gallery type, width and height of gallery and images, image distance and radius. It is already possible to add border for images, enable shadow option and choose the theme for popup image. On the Captions tab you can enable caption option and set different settings of it.

Don't forget to press the "Save" button. After all these points you can see the result in the left top corner (you have the ability to choose image for preview) or simply click on "Preview" button and will be able to see the whole gallery. 
Under image preview you can see the shortcode. Copy and paste the shortcode into your post or page and Gallery by Supsystic plugin will automatically create the gallery on your page using the settings you choose above.
Important! Shortcode must be inserted in a text editor page, and not in the visual.

= To install gallery plugin via FTP, you must =

1. Download the photo gallery plugin
2. Unarchive the plugin supsystic-grid-gallery.zip
3. Copy the folder with plugin
4. Open ftp \wp-content\plugins\
5. Paste the plug-ins folder in the folder
6. Go to admin panel => open item "Plugins" => activate the gallery plugin

== Other Notes ==

= How to Create an Photo Gallery in WordPress =
  
= Step 1: Creating Responsive Grid Gallery =

1. On the left navigation menu click “New Gallery”.

2. Enter the name of gallery. 

3. Choose gallery template. You have the ability to choose one of 4 templates:

 * Standard Gallery (with fixed grid).
 * Vertical Gallery (without distance between images).
 * Rounded Gallery (with fixed grid and border option).
 * Horizontal Gallery (with shadow option).
 
4.  Click "Save" button.

On the next steps you can change the settings of gallery template.

= Step 2: Adding images to the Grid Photo Gallery =

With responsive photo gallery plugin you can import images from - 

 * WordPress Media Library
 * Instagram Account
 * Flickr Account (PRO feature)
 * Tumblr Account (PRO feature)
 * Facebook Account (PRO feature)
 
1. Click on "Add Images" button.

2. Choose images from WordPress Media Library - mark those images that you like or need. If there are no images that you need, click the Upload Files tab - here you can drop files anywhere to upload or select files from your computer.

3. Click on “Choose Image” button. 

4. To manage or change properties of images click “Image List” button. Here you can:

 - change the order of images - simply by dragging them manually;
 - delete images;
 - add new images from different sources to the grid gallery - click “Add Images” button and select source to import from;
 - add caption to gallery image - it will be displayed on the caption effect of the gallery; 
 - add SEO - tags for search requests;
 - attach links to image - it will go to the link when you click the image;
 - attach video - it will be displayed in a pop-up image when you click on a picture;
 - add tags for image categories.
 
= Step3: Editing of Photo Gallery settings =

To get back to the settings of gallery - сlick “Properties” button.

Here you can change default options of photo gallery template. Simply navigate the corresponding tab of properties - Main, Captions, Categories and Posts. Important! After changing the settings of gallery don’t forget to click “Save” button.

= 1. Main tab =

1.1. Gallery Type - here you can:
	
 - choose the type of gallery - there are 3 gallery types: Fixed, Horizontal, Vertical;
 - set the distance between gallery images;
 - set the width of the responsive image gallery;
 - set the width and height of images;
 - set the radius for picture - thanks to this option will turn out wonderful rounded or circular galleries.
 
All the values of these options, except for images distance, can be set in pixels or in percent. Images distance set only in pixels.

1.2. Border type - here you have the opportunity:
	
 - select 4 different types of gallery  border - Solid, Dotted, Dashed, Double;
 - select the color of border for images;
 - define the border width.
 
This option can be disabled. Simply select value - “None” for Border Type.

1.3. Shadow 
	
Activate “Available” radio button to see the settings of this option. Shadow option of Gallery by Supsystic  allows you:

 - show the shadow when mouse is over the image. there are two types of this option: show / hide the shadow when mouse is on picture;
 - overlay image with shadow ;
 - choose the color of shadow;
 - set the shadow blur in percent;
 - specify the offset of the shadow by X and Y.

1.4. Pop-up Image - here you can:
	
 - choose the “Big Image” theme for popup image of grid gallery - to do this you need simply click a “Choose theme” button and select one of the great themes. One of the themes include the navigation control with thumbnails of images, which is much simplify and speed up access to all images of the gallery.
 - select the transition of pop-image - Fade, Elastic, None.
 - enable the slideshow option - you need activate “Available” radio button to see the settings of this option. here you can set speed of slideshow and activate slideshow autostart. If you activate it - slideshow starts when big image open in popup, if don’t - you have the ability to start slideshow whenever you want. In any case, on the big image will be slideshow controls.

= 2. Captions tab =
 
2.1. Captions - this option contains such features as:
	
 - a huge variety of gallery caption effects. Simply click “Choose effect” button and select the overlay effect. 
 - the ability to choose background color of caption 
 - text color of caption option
 - the ability to set the level of transparency for caption option
 - the ability to specify the font size of the text. (in pixels, percent, ems)
 - the ability to choose the text alignment of caption - left, center, right, auto.
 
2.2. Icons (PRO feature) - show amazing icons on the images with different options. There are several types of icons, it all depends on what exactly is attached to the picture - link, video or anything.
	
 - select the effect for animation of icons - you need to click ”Animation” button and choose the effect.
 - select the icons color.
 - select the icons hover color - color of the icon when mouse is over.
 - choose the image gallery background color for icons.
 - choose the photo gallery background hover color for icons.
 - set the size of icons.
 - set the distance between icons.
 - enable the overlay effect - image will fade under the overlay.
 - set the color and level of transparency for overlay effect of icons.
 
You have the opportunity to enable Captions and Icons options at the same time, but then you can not choose caption effect for gallery. “Appear” effect always will be by default.

= 3. Categories tab =

3.1. Show slideshow gallery categories (PRO feature) - categorize images in the grid gallery.
	
 First you need to add tags for the categories:
 
	1. Click on “Images list”.
	2. For each photo choose Categories tab. 
	3. Add a tag for all images.
	4. Go back to the settings - click “Properties” button.
	
Further activate “Available” radio button responsive photo gallery and definitely select the preset (without it categories not displayed). You have the ability to choose one of the default presets or create your own. 

In order to create the preset click “Show preset editor” button. Provide a name for your photo gallery template and specify the settings that you need. Here you can:

 - Select the background color for container or hide it. Сontainer - the area where will be shown the categories of the image gallery.
 - Select the background color for text or hide it.
 - Select the color of text or use color, based on your theme.
 - Set overall vertical and horizontal padding for the categories.
 - Set the font weight and size.
 - Define the border width for the categories
 - Select the border type.
 - Choose the border color.
 - Define the border radius for the categories.
 
After configuring the image gallery settings click “Save” button. Select your preset in the “Choose preset” dropdown box.

Also Categories option includes such capabilities:

 * Enable shuffling animation of images. (If you enable posts layout on the Posts tab - this feature will not be available)
 * Set the duration of animation.
 * Define the position of categories - over or under the gallery.
 * Determine the alignment of categories.

3.2. Pagination (PRO feature) - make your image gallery easier to use with grid gallery wordpress plugin. 
	
At first you need to set the number of images per page and choose the preset. You can choose one of the default presets or create your own. In order to create the preset click “Show preset editor” button. Enter the name of your preset and specify the settings that you need. They are exactly the same as in the previous option. After configuring the settings click “Save” button. Select your preset in the “Choose preset” dropdown box.

As well you have the opportunity:

 * Set the position of buttons - top or bottom.
 * Define the alignment of pagination.
 * You can select only the one of these two options (Categories or Pagination) for one gallery.
 * Pagination option is not available, when is set vertical type of gallery. 

= 4. Posts tab (PRO feature) - show posts and pages with Gallery by Supsystic! = 

This tab is completely about the capabilities to display the posts and pages in the gallery. To activate the posts option - select “Enable” in Posts Layout dropdown list. Here you find such features as: 

 - the ability to choose one of 3 post layout styles - Fixed, Animated, Cover; 
 - the ability to add pages and posts - simply select post / page from corresponding dropdown list and click “Add post” / “Add page” button - it will appear on the bottom of the page;
 - the opportunity to choose what to show on the posts layout in the gallery and what not to show - author of the post, date, contents and categories of the posts;
 - the ability to delete a post or page from the grid gallery.

= Step 4: Photo gallery preview settings =

After you change some settings -  with responsive photo gallery plugin, you can immediately see the result in a live preview in the left top corner. You have the ability to choose image for preview - simply click the “Choose image for preview” button under the window of live preview.

Live photo gallery preview allows you to view the setting of such features:

 * Images radius 
 * Border option
 * Shadow option
 * Caption effect
 
In order to see how the other options of responsive image gallery look - you should to click on “Preview” button and will be able to see the whole gallery in a new tab of your browser.

= Step 5:  Displaying the Photo Gallery on the website =

Under the window with image preview you can see the shortcode of gallery. Copy and paste the shortcode into your post or page and Gallery by Supsystic plugin will automatically create the gallery on your page using the settings you choose above.
Important! Photo gallery plugin shortcode must be added in a text editor page, and not in the visual.

*Check other WordPress plugins:*

* [Social Share Buttons by Supsystic](https://wordpress.org/plugins/social-share-buttons-by-supsystic/ "Social Share Buttons by Supsystic")
* [Data Tables Generator by Supsystic](https://wordpress.org/plugins/data-tables-generator-by-supsystic/ "Data Tables Generator by Supsystic")
* [Google Maps Easy](https://wordpress.org/plugins/google-maps-easy/ "Google Maps Easy")
* [Gallery by Supsystic](https://wordpress.org/plugins/gallery-by-supsystic/ "Gallery by Supsystic")
* [Backup by Supsystic](https://wordpress.org/plugins/backup-by-supsystic/ "Backup by Supsystic")
* [Lightbox by Supsystic](https://wordpress.org/plugins/lightbox-by-supsystic/ "Lightbox by Supsystic")
* [Slider by Supsystic](https://wordpress.org/plugins/slider-by-supsystic/ "Slider by Supsystic")
* [PopUp by Supsystic](https://wordpress.org/plugins/popup-by-supsystic/ "PopUp by Supsystic")
* [Security by Supsystic](https://wordpress.org/plugins/security-by-supsystic/ "Security by Supsystic")
* [Secure Login by Supsystic](https://wordpress.org/plugins/secure-login-by-supsystic/ "Secure Login by Supsystic")

== Screenshots ==

1. [Fixed Gallery.](http://supsystic.com/plugins/photo-gallery/#examples?utm_source=wordpress&utm_medium=screenshots&utm_campaign=gallery "Fixed Gallery") It's still responsive and compatible all mobile devices, but fixed :)
2. [Photo Gallery WordPress Plugin](http://supsystic.com/plugins/photo-gallery?utm_source=wordpress&utm_medium=screenshots&utm_campaign=gallery "Photo Gallery WordPress plugin") by Supsystic the complete set for any user that can assist quickly create stunning online presentation, photo gallery or slideshow
3. Gallery plugin admin area. Choose one of the professional image gallery template and manage any aspect of the gallery with easy-to-use options
4. [Post Feed Gallery](http://supsystic.com/post-feed-animated-gallery-example?utm_source=wordpress&utm_medium=screenshots&utm_campaign=gallery "Post Feed Gallery") - it never was so easy to show Posts, Pages or any Content in an amazing view of the post feed gallery.
5. [Image Circle Gallery](http://supsystic.com/plugins/photo-gallery/#examples?utm_source=wordpress&utm_medium=screenshots&utm_campaign=gallery "Image Circle Gallery") - perform your best ideas with Gallery by Supsystic!

== Changelog ==

= 1.4.2 / 01.09.2015 =

 * Fix safari bug when image not fit in container and left empty spaces
 * Links title attribute tooltips now not showing on hover
 * Minor issues fix

= 1.4.1 / 28.08.2015 =

 * Images now aligned according to the shortcode value
 * Fixed image size bugs in "fixed column" gallery
 * Fixed bugs with "fixed column" gallery when pagination enabled
 * Fixed bug when images drops out from a parent container

= 1.4.0 / 26.08.2015 =

 * Fixed "Vertical gallery" bug

= 1.3.9 / 25.08.2015 =

 * Fixed "Enable shuffling" bug, not saving state properly
 * Fixed bugs with "Fixed Columns" gallery type now displays properly
 * Fixed bugs with shuffling animation
 * Added new border styles: groove, ridge, inset, outset
 * Updated some help tooltips 

= 1.3.8 / 19.08.2015 =

 * Fix for gallery categories update categories order list when adding or removing category
 * Post and page categories also can add to order list
 * Added feature to change All button order
 * Fix post feed gallery

= 1.3.7 / 18.08.2015 =

 * Gallery borders fix
 * Add tooltips and FAQs
 * Minor issues fix

= 1.3.6 / 14.08.2015 =

 * Fix for unescaping quotes in tags, and remove deleted categories from categories order list
 * Add filter for empty categories
 * Minor issues fix

= 1.3.5 / 13.08.2015 =

 * Fixed facebook session notices & margins in categories
 * Fixed Caption – Direction Aware effect
 * Hide overflow inline property (due to a bug with shadow cutting)
 * Fix for 'sticky' margin bug
 * Fixed firefox 'flat' effect bug and caption offset in chrome when shadow is on
 * Added msg & import dialog when gallery is empty
 * Fix for IE rotate3d bug for cube effect
 * Added feature in categories to remove 'all category' button
 * Fix faq link
 * Added showing spinner icon on creating gallery
 * Remove round corners from dialogs
 * Fix dialog close button
 * Replace old lighten bg color method that caused falling out of the valid color hex range
 * Add border:none to images-links ot prevent styling with other themes
 * Fix categories/pagianation toggle state
 * Rename radiobutton Available to Enable in options
 * Default animation duration set to 1800
 * Added feature to change order of the categories
 * Popup Image – Hide Popup Captions now working fix

= 1.3.4 / 06.08.2015 =

 * Fixed template names
 * Additional Font setting existence check in admin area
 * Added Polish language
 * Fix Popup Image (lightbox) for galleries with Categories.
 * CSS: added clarifying selector to avoid conflicts with other plugins.
 * CSS: added 100% height rule for normal pic rendering in Chrome (with 100% gallery width set).
 * Fixed position attribute
 * Code review and minor issues changes

= 1.3.3 / 24.07.2015 =

 * Updated framework
 * Menu anti-duplication fixed
 * Fix invalid photos margin when fixed position and distance set
 * CSS: added clarifying selector to avoid conflicts with other plugins

= 1.3.2 / 14.07.2015 =

 * Fixed error while saving gallery settings

= 1.3.1 / 09.07.2015 =

 * Fixed social import functionality
 * Checkall buttons for social import fixed
 * Notices fixed
 * Saving settings console errors removed
 * Thumbs saving notices fixed
 * Categories and pagination presets selection notices fixed
 * Changes in Overview page
 * Minor changes

= 1.3.0 / 11.06.2015 =

 * Presets check fixed
 * Updated Hebrew language
 * Updated Portuguese (Brazil) and Finnish languages
 * Customized jGrowl popup messages
 * Font loader checks added
 * Shortcode fixes
 * Changes in Overview page
 * Functional fixes

= 1.2.9 / 11.06.2015 =

 * Gallery code optimizations, changes in the appearance
 * Shortcodes fix, submenu anti-duplicate
 * Fixes for social images import
 * Social import breadcrumbs fixed
 * Languages added, new default language file
 * Photo Gallery column number fixed
 * Figcaption height calculation fixed
 * iChek fixed in admin area tabs, fixed columns layout changed
 * Minor bugs fix
 
= 1.2.8 / 28.05.2015 =

 * Hor fixes
 * Revolving effects fixed
 * Minor changes

= 1.2.7 / 28.05.2015 =

 * Added new categories & pagination presets
 * Categories & pagination presets moved to dialogs
 * Modified settings for standard categories & pages presets
 * Caption height fixed for new settings
 * Added Hebrew(Israil) language
 * Functional fixes
 * Minor changes

= 1.2.6 / 13.05.2015 =

 * New cube transition effect
 * Modified caption and gallery 'position' option - fixed better vertical alignment
 * Translation file updated
 * Responsive gallery bug fix
 * Minor bugs fix

= 1.2.5 / 07.05.2015 =

 * PhpCode layout fixed
 * Caption corrections
 * Minor changes

= 1.2.4 / 07.05.2015 =

 * Change the appearance of gallery shortcodes for easy copying
 * Changes & bug-fixing in gallery caption text-align option
 * Added translations to the French, Dutch, Finnish, Portuguese (Brazil)
 * Fixed saving the preset
 * Added option to select caption position
 * Bug fixes

= 1.2.2 / 23.04.2015 =

 * Hot fixes for new gallery type
 * Minor changes

= 1.2.1 / 22.04.2015 =

 * Added ability to edit custom gallery presets
 * Fix for older PHP versions

= 1.1.9 / 21.04.2015 =

 * Gallery functional changed
 * Minor fixes
 * New gallery type
 * Attachments load optimization, position load optimization, overview news link changed
 * Google fonts added to captions option
 * New gallery themes added
 * Finnish language added

= 1.1.8 / 08.04.2015 =

 * Functional changed
 * Minor fixes
 * Captions with HTML saving changed
 * Popup sizes calculations on mobile devices with vimeo videos changed
 * Galleries list fixed

= 1.1.7 / 1.04.2015 =

 * Direction aware effect caption changed
 * Functional changed
 * Minor fixes
 * Shortcode insertion button added to tinyMCE editor
 * Shortcode selection dialog added

= 1.1.6 / 27.03.2015 =

 * New effects and presets added
 * Functional changed
 * Minor fixes

= 1.1.5 / 25.03.2015 =

 * Hot fixes

= 1.1.4 / 24.03.2015 =

 * New option Popup Image disable option
 * Gallery overview news fixed
 * Post Feed Gallery notice fixed
 * HTML caption add

= 1.1.4 / 24.03.2015 =

 * New option Popup Image disable option
 * Gallery overview news fixed
 * Post Feed Gallery notice fixed
 * HTML caption add
 
= 1.1.3 / 17.03.2015 =

 * External image link added
 * Minor issues fix

= 1.1.2 / 16.03.2015 =

 * Debug mode warning fixes
 * Option to toggle popup on mobile added
 * Some spelling issues fixed (including php tag in insert code generation)
 * Other minor issues fix

= 1.1.1 / 11.03.2015 =

* Overview page php 5.2 support added
* Minor bugs fixed
* Functional fixes

= 1.1.0 / 09.03.2015 =

* Image upload fixed
* Gallery shortcode fixed
* Add Overview page
* Minor bugs fixed

= 1.0.21 / 6.03.2015 =

* Minor changes
* Gallery functional fixes
* Overview page added
* Supsystic news added
* Common FAQs
* Mail form added
* Video tutorial
* Server configs

= 1.0.20 / 26.02.2015 =

* Minor changes
* Functional fixes

= 1.0.19 / 20.02.2015 =

* Responsive galery on Iphone fixed
* Default gallery settings fixed
* Empty gallery page changed
* Standart template settings changed
* Choosing preview image from empty images fixed
* Social import from instagram added
* Photo gallery templates fixed
* Photo gallery plugin by Supsystic compatibility with WordPress 4.1.1

= 1.0.18 =

* Functional fixes
* Minor issues fixes
* Shadow settings fixes

= 1.0.17 =

* Functional fixes
* Core code improvements
* Installation optimizations
* Minor issues fixes

= 1.0.16 =

* Functional fixes

= 1.0.15 = 

* Submit to WordPress.org