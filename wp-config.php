<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'instaura_brandsout_com');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|Xji|z#c2Q38v/6GnnSC?? -df#.!J.>qaB^cV3nr{Vl1&^gUxq4A^`Bv@c!S?lT');
define('SECURE_AUTH_KEY',  '}lg-m$>E Pn-B+?P-8W2mmA+-]+^i.t.!=F2h!W08=pvh$b)VXSxA#6}`wuOC{,O');
define('LOGGED_IN_KEY',    'n7ya+ze4V-V4sA7GU>3&N./MQgN3eZ{hQP-^1n7GxtJs+;Z C*Mj$|F$ED(G*SZg');
define('NONCE_KEY',        'mX=QT _&&^YclE49/h*(nf:/ciWIs~m3+jm;<-KH-/k*F_v]# .j4ZBGN2`h3=3Y');
define('AUTH_SALT',        'Ks)&kpL|xnFfApP:;w^:2@>.BEPbBCM0pPH)Uveb/9BrMo*`ZbtaTs^IL^uW~qtB');
define('SECURE_AUTH_SALT', '~;rp@ Cx#|K5%sBsAc8XRpTdvS7rc|}Eu<_L1lwoeS&uCNy |HBJ98KjH`_)?OAw');
define('LOGGED_IN_SALT',   'Pa+?j!^D#(uA@@7j$tU%F`+EA[+v/`@TBJ)EU(#Yq+l1cBNX)FmvZrHm?JI$:rS ');
define('NONCE_SALT',       'C7+m%tew:%/@59OTbIM(lJt:E}Gzz=`07*vkX5+38<{/8zTA#cjGIe9ORHp[.$J6');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_8i23yd_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
